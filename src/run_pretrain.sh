HF_DATASETS_CACHE=/data/cache \
TRANSFORMERS_CACHE=/data/cache \
python run_experiment.py \
    --model_name_or_path facebook/bart-base \
    --data data/dbpedia_query_orca_query_log_openwebtext_sythetic_20220627.csv \
    --val_data data/top10000.csv \
    --test_data data/testing_data_20220630.csv \
    --dynamic_noise True \
    --max_steps 400000 \
    --batch_size 128 \
    --learning_rate 2e-4 \
    --gradient_accumulation_steps 8 \
    --experiment_name bart_base_pretrain \
    --report_to tensorboard \
    --fp16 True
