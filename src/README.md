# Spelling Correction with Seq2seq Denoising Transformer

## Usage
### Download dataset from s3
```bash
# dataset is at wtg's aws account
aws s3 cp s3://orca-lab-bucket/spell_correct_dataset/spell_correct_dataset.zip spell_correct_dataset.zip
unzip spell_correct_dataset.zip -d data/
```
### pretrain
```bash
./run_pretrain.sh
```

### monitoring
```
tensorboard --logdir logs/bart_base_pretrain
```
