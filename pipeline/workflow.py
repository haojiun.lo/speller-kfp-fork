import os
import kfp
import kfp.dsl as dsl

from kfp import components

components.load_component_from_url()

download_s3_op = components.load_component_from_file("components/yaml_files/download_dvc_data_from_s3.yaml")

trainer_op = components.load_component_from_file("examples/seq2seq_speller/components/trainer/trainer.yaml")

display_metrics_op = components.load_component_from_file("components/yaml_files/display_metrics.yaml")

exporter_op = components.load_component_from_file("examples/seq2seq_speller/components/exporter/exporter.yaml")

upload_op = components.load_component_from_file("components/yaml_files/upload_to_s3.yaml")


@dsl.pipeline(
    name="orca-speller-pipeline",
    description="pipeline for pre-training/finetune seq2seq speller",
)
def speller_pipeline(
    git_repo,
    git_rev,
    data_path,
    model_name_or_path,
    dynamic_noise,
    max_steps,
    batch_size,
    learning_rate,
    gradient_accumulation_steps,
    fp16,
    bucket_name_for_uploading_exp_working_dir,
    s3_key_for_uploading_exp_working_dir
):
    # 1.download data from s3
    data_download = download_s3_op(git_repo, git_rev, data_path)

    # 2.model training job
    train_task = (
        trainer_op(
            model_name_or_path=model_name_or_path,
            data_path=data_download.outputs["output_path"],
            dynamic_noise=dynamic_noise,
            max_steps=max_steps,
            batch_size=batch_size,
            learning_rate=learning_rate,
            gradient_accumulation_steps=gradient_accumulation_steps,
            fp16=fp16,
        )
        .set_gpu_limit(1)
        .set_memory_request('16G')
        .set_cpu_request("1000m")
        .set_cpu_limit("2500m")
    )

    # 3.display testing metrics
    _ = display_metrics_op(train_task.outputs["output_path"]).after(train_task)

    # 4.model exporting
    export_task = (
        exporter_op(save_path=train_task.outputs["output_path"])
        .set_memory_request('4G')
        .set_memory_limit('8G')
    ).after(train_task)

    # 5.upload to s3
    # upload training ckpt, logs, params to s3
    _ = upload_op(
        bucket_name_for_uploading_exp_working_dir,
        s3_key_for_uploading_exp_working_dir,
        train_task.outputs["output_path"]).after(train_task)

    # upload exported model to s3
    _ = upload_op(
        bucket_name_for_uploading_exp_working_dir,
        s3_key_for_uploading_exp_working_dir,
        export_task.outputs["export_path"]).after(export_task)


if __name__ == "__main__":
    file_dir = os.path.dirname(__file__)
    yaml_file = os.path.join(file_dir, "speller-pipeline.yaml")
    kfp.compiler.Compiler().compile(
        pipeline_func=speller_pipeline, package_path=yaml_file
    )
